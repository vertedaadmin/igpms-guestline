﻿namespace IGPMS
{
    using System;

    public class RevenueToAnalysis
    {
        private string _classID = "";
        private int _profitCenter;
        private int _typeID;
        private int _mealID;
        private string _analCode = string.Empty;

        public string ClassID
        {
            get
            {
                return this._classID;
            }

            set
            {
                this._classID = value;
            }
        }

        public int ProfitCenter
        {
            get
            {
                return this._profitCenter;
            }

            set
            {
                this._profitCenter = value;
            }
        }

        public int TypeID
        {
            get
            {
             return   this._typeID;
            }
            set
            {
                this._typeID = value;
            }
        }

        public int MealID
        {
            get
            {
                return this._mealID;
            }

            set
            {
                this._mealID = value;
            }
        }

        public string AnalCode
        {
            get
            {
                return this._analCode;
            }

            set
            {
                this._analCode = value;
            }
        }
    }
}

