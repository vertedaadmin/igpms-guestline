﻿namespace IGPMS
{
    using System;

    public class ConfigSettings
    {
        private bool _error;
        private string _siteID = "";
        private int _interfaceID;
        private string _operatorCode = "";
        private string _password = "";
        private string _url = "";

        public bool Error
        {
            get
            {
                return this._error;
            }
            set
            {
                this._error = value;
            }
        }

        public string SiteID
        {
            get
            {
             return   this._siteID;
            }
            set
            {
                this._siteID = value;
            }
        }

        public int InterfaceID
        {
            get
            {
                return this._interfaceID;
            }
            set
            {
                this._interfaceID = value;
            }
        }

        public string OperatorCode
        {
            get
            {
                return this._operatorCode;
            }
            set
            {
                this._operatorCode = value;
            }
        }

        public string Password
        {
            get
            {
                return this._password;
            }
            set
            {
                this._password = value;
            }
        }

        public string Url
        {
            get
            {
                return this._url;
            }
            set
            {
                this._url = value;
            }
        }
    }
}

