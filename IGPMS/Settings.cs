﻿namespace MenuSync
{
    using System;

    internal class Settings
    {
        private string mailToAddress = "";
        private string mailFromAddress = "";
        private string password = "";
        private string siteName = "";

        public string SiteName
        {
            get
            {
                return this.siteName;
            }
            set {  
                this.siteName = value;
        }
        }

        public string Password
        {
            get
            {
                return this.password;
            }
            set
            {
                this.password = value;
            }
        }

        public string MailFromAddress
        {
            get
            {
                return this.mailFromAddress;
            }
            set
            {
                this.mailFromAddress = value;
            }
        }

        public string MailToAddress
        {
            get
            {
                return this.mailToAddress;
            }
            set
            {
                this.mailToAddress = value;
            }
        }
    }
}

