﻿namespace IGPMS
{
    using System;

    public class PMSLogInResponse
    {
        private bool _status = true;
        private string _errorText = string.Empty;
        private string _session_id = "0";

        public bool status
        {
            get
            {
                return this._status;
            }
            set
            {
                this._status = value;
            }
        }

      

        public string errorText
        {
            get {
                return this._errorText;

            }
            set
            {
                this._errorText = value;
            }
        }

        public string Session_id
        {
            get
            {
                return this._session_id;
            }

            set
            {
                this._session_id = value;
            }
        }
    }
}

