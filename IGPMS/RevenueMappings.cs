﻿namespace IGPMS
{
    using System;
    using System.Collections.Generic;

    public class RevenueMappings
    {
        private bool _error;
        private List<RevenueToAnalysis> _revenueCodes = new List<RevenueToAnalysis>();

        public bool Error
        {
            get
            {
                return this._error;
            }

            set
            {
                this._error = value;
            }
        }

        public List<RevenueToAnalysis> RevenueCodes
        {
            get
            {
                return this._revenueCodes;
            }
            set
            {
                this._revenueCodes = value;
            }
        }
    }
}

