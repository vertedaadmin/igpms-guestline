﻿// Decompiled with JetBrains decompiler
// Type: MenuSync.VertedaDiagnostics
// Assembly: IGPMS3, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4F069CC3-1715-4110-B87E-9B6BDAD4366F
// Assembly location: C:\Users\southwortht\Desktop\Guestline\IGPMS3.dll

using Agilysys_Code_Library.Logging;
using System;
using System.Net;
using System.Net.Mail;
using System.Net.Sockets;
using System.Reflection;
using System.Xml;

namespace MenuSync
{
  internal class VertedaDiagnostics
  {
    public string logFileName = "";
    private static string newMessage;

    public static Settings GetStaticSettings()
    {
      Settings settings = new Settings();
      try
      {
        string str = "C:\\tsm sync";
        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.Load(str + "\\diagnostics_config.xml");
        foreach (XmlNode selectNode in xmlDocument.SelectNodes("//site_name"))
          settings.SiteName = selectNode.InnerText;
        foreach (XmlNode selectNode in xmlDocument.SelectNodes("//mail_from_address"))
          settings.MailFromAddress = selectNode.InnerText;
        foreach (XmlNode selectNode in xmlDocument.SelectNodes("//mail_to_address"))
          settings.MailToAddress = selectNode.InnerText;
        foreach (XmlNode selectNode in xmlDocument.SelectNodes("//key"))
          settings.Password = selectNode.InnerText;
      }
      catch (Exception ex)
      {
      }
      return settings;
    }

    public static void ErrorMailer(string strMessage)
    {
      VertedaDiagnostics.LogMessage("Sending error report email", "I", true);
      Settings staticSettings = VertedaDiagnostics.GetStaticSettings();
      try
      {
        MailAddress from = new MailAddress(staticSettings.MailFromAddress, "Verteda Error Reporting");
        MailAddress to = new MailAddress(staticSettings.MailToAddress, "To Name");
        string password = staticSettings.Password;
        string str1 = "V-E-R: " + staticSettings.SiteName + " (" + Assembly.GetExecutingAssembly().GetName().Name + ")";
        string str2 = strMessage;
        SmtpClient smtpClient = new SmtpClient()
        {
          Host = "smtp.gmail.com",
          Port = 587,
          EnableSsl = true,
          DeliveryMethod = SmtpDeliveryMethod.Network,
          UseDefaultCredentials = false,
          Credentials = (ICredentialsByHost) new NetworkCredential(from.Address, password)
        };
        MailMessage message = new MailMessage(from, to);
        message.Subject = str1;
        message.Body = str2;
        try
        {
          message.Attachments.Add(new Attachment("screenshot.png"));
        }
        catch (Exception ex)
        {
        }
        smtpClient.Send(message);
      }
      catch (Exception ex)
      {
      }
      VertedaDiagnostics.LogMessage("Sent error report email", "I", true);
    }

    public static void VertedaErrorHandler(string errorDetails, bool sendReport, bool notifyUser)
    {
      VertedaDiagnostics.LogMessage("-------------ERROR:" + errorDetails, "E", true);
      VertedaDiagnostics.LogMessage("STARTED ERROR HANDLING", "I", true);
      string str = "Application Error Report" + Environment.NewLine + Environment.NewLine + "Application Name:" + Assembly.GetExecutingAssembly().GetName().Name + Environment.NewLine + Environment.NewLine + "Appication Version:" + Assembly.GetExecutingAssembly().GetName().Version.ToString() + Environment.NewLine + Environment.NewLine + "Terminal IP Address:";
      foreach (IPAddress address in Dns.GetHostEntry(Dns.GetHostName()).AddressList)
      {
        if (address.AddressFamily == AddressFamily.InterNetwork)
          str = str + address.ToString() + ", ";
      }
      string[] strArray = new string[6]
      {
        str + Environment.NewLine,
        "Error occured:",
        null,
        null,
        null,
        null
      };
      DateTime now = DateTime.Now;
      strArray[2] = now.ToShortDateString();
      strArray[3] = "  ";
      now = DateTime.Now;
      strArray[4] = now.ToShortTimeString();
      strArray[5] = Environment.NewLine;
      string strMessage = string.Concat(strArray) + Environment.NewLine + Environment.NewLine + "Error details:" + Environment.NewLine + errorDetails + Environment.NewLine + Environment.NewLine + "END OF MESSAGE" + Environment.NewLine;
      if (sendReport)
        VertedaDiagnostics.ErrorMailer(strMessage);
      int num = notifyUser ? 1 : 0;
      VertedaDiagnostics.LogMessage("ENDED ERROR HANDLING", "I", true);
    }

    public static void VertedaExceptionHandler(
      Exception ex,
      bool sendReport,
      bool notifyUser,
      bool shutdown)
    {
      VertedaDiagnostics.LogMessage("-------------ERROR:" + ex.Message, "E", true);
      VertedaDiagnostics.LogMessage(ex.StackTrace, "E", true);
      VertedaDiagnostics.LogMessage("STARTED ERROR HANDLING", "I", true);
      string str = "Application Exception Report" + Environment.NewLine + Environment.NewLine + "Application Name:" + Assembly.GetExecutingAssembly().GetName().Name + Environment.NewLine + Environment.NewLine + "Appication Version:" + Assembly.GetExecutingAssembly().GetName().Version.ToString() + Environment.NewLine + Environment.NewLine + "Terminal IP Address:";
      foreach (IPAddress address in Dns.GetHostEntry(Dns.GetHostName()).AddressList)
      {
        if (address.AddressFamily == AddressFamily.InterNetwork)
          str = str + address.ToString() + ", ";
      }
      string strMessage = str + Environment.NewLine + "Exception occured:" + DateTime.Now.ToShortDateString() + "  " + DateTime.Now.ToShortTimeString() + Environment.NewLine + Environment.NewLine + Environment.NewLine + "Exception details:" + Environment.NewLine + ex.Message + Environment.NewLine + Environment.NewLine + "Exception Stack Trace:" + Environment.NewLine + ex.StackTrace + Environment.NewLine + Environment.NewLine + "Exception Source:" + Environment.NewLine + ex.Source + Environment.NewLine + Environment.NewLine + "END OF MESSAGE" + Environment.NewLine;
      if (sendReport)
        VertedaDiagnostics.ErrorMailer(strMessage);
      if (!shutdown)
      {
        int num = notifyUser ? 1 : 0;
      }
      VertedaDiagnostics.LogMessage("ENDED ERROR HANDLING", "I", true);
    }

    public static void VertedaUHExceptionHandler(object sender, UnhandledExceptionEventArgs args)
    {
      Exception exceptionObject = (Exception) args.ExceptionObject;
      VertedaDiagnostics.LogMessage("-------------ERROR:" + exceptionObject.Message, "E", true);
      VertedaDiagnostics.LogMessage(exceptionObject.StackTrace, "E", true);
      VertedaDiagnostics.LogMessage("STARTED ERROR HANDLING", "I", true);
      string str = "Application Unhandled Exception Report" + Environment.NewLine + Environment.NewLine + "Application Name:" + Assembly.GetExecutingAssembly().GetName().Name + Environment.NewLine + Environment.NewLine + "Appication Version:" + Assembly.GetExecutingAssembly().GetName().Version.ToString() + Environment.NewLine + Environment.NewLine + "Terminal IP Address:";
      foreach (IPAddress address in Dns.GetHostEntry(Dns.GetHostName()).AddressList)
      {
        if (address.AddressFamily == AddressFamily.InterNetwork)
          str = str + address.ToString() + ", ";
      }
      VertedaDiagnostics.ErrorMailer(str + Environment.NewLine + "Exception occured:" + DateTime.Now.ToShortDateString() + "  " + DateTime.Now.ToShortTimeString() + Environment.NewLine + Environment.NewLine + Environment.NewLine + "Exception details:" + Environment.NewLine + exceptionObject.Message + Environment.NewLine + Environment.NewLine + "Exception Stack Trace:" + Environment.NewLine + exceptionObject.StackTrace + Environment.NewLine + Environment.NewLine + "Exception Source:" + Environment.NewLine + exceptionObject.Source + Environment.NewLine + Environment.NewLine + "END OF MESSAGE" + Environment.NewLine);
      VertedaDiagnostics.LogMessage("ENDED ERROR HANDLING", "I", true);
    }

    public static event EventHandler MessageFired;

    public static string NewMessage
    {
      get
      {
        return VertedaDiagnostics.newMessage;
      }
      set
      {
        VertedaDiagnostics.newMessage = value;
        VertedaDiagnostics.MessageFired((object) VertedaDiagnostics.newMessage, new EventArgs());
      }
    }

    public static void OpenLog(string logLocation, string logFileName)
    {
      TextFileLogging.open_logfile(logLocation, logFileName);
    }

    public static void CloseLog()
    {
      TextFileLogging.close_logfile();
    }

    public static void LogMessage(string message, string messsageType, bool Broadcast)
    {
      try
      {
        TextFileLogging.write_to_log(message, messsageType);
        if (!Broadcast)
          return;
        VertedaDiagnostics.NewMessage = DateTime.Now.TimeOfDay.ToString() + " : " + message;
      }
      catch (Exception ex)
      {
      }
    }
  }
}
