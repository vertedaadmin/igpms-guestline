﻿// Decompiled with JetBrains decompiler
// Type: InfoGenesis.PublishedInterfaces.Contracts.PMS_XMLContract.GuestInfo
// Assembly: PMS_XMLContract, Version=4.4.1.2, Culture=neutral, PublicKeyToken=null
// MVID: 1A6B76FD-4336-4474-B1B4-3C18D74F5E0E
// Assembly location: C:\Users\Tony\Desktop\PMS XML\PMS_XMLContract.dll

using System;

namespace InfoGenesis.PublishedInterfaces.Contracts.PMS_XMLContract
{
  [Serializable]
  public class GuestInfo
  {
    public string Level;
    public string ArrivalDate;
    public string DepartureDate;
    public string CreditType;
    public string CreditLimit;
    public GuestResource Group;
    public GuestResource Package;
    public GuestResource MealPlan;
  }
}
