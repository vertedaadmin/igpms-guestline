﻿// Decompiled with JetBrains decompiler
// Type: InfoGenesis.PublishedInterfaces.Contracts.PMS_XMLContract.LookupResponse
// Assembly: PMS_XMLContract, Version=4.4.1.2, Culture=neutral, PublicKeyToken=null
// MVID: 1A6B76FD-4336-4474-B1B4-3C18D74F5E0E
// Assembly location: C:\Users\Tony\Desktop\PMS XML\PMS_XMLContract.dll

using System;
using System.Xml.Serialization;

namespace InfoGenesis.PublishedInterfaces.Contracts.PMS_XMLContract
{
  [XmlRoot("Response")]
  [Serializable]
  public class LookupResponse
  {
    public int ErrorCode;
    public string ErrorDisplayMessage;
    public string ErrorDebugMessage;
    public bool MoreRecordsFlag;
    public string MoreRecordsKey;
    public AcntCollection MatchingAccounts;

    public LookupResponse()
    {
      this.MatchingAccounts = new AcntCollection();
    }
  }
}
