﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyKeyName("")]
[assembly: AssemblyProduct("InfoGenesis POS")]
[assembly: AssemblyDelaySign(false)]
[assembly: AssemblyDescription("PMS Host Data Exchange Spec.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Agilysys")]
[assembly: AssemblyTitle("PMS_XMLContract Class Library")]
[assembly: AssemblyCopyright("Copyright © 2003 - 2013 Agilysys, Inc. All rights reserved.")]
[assembly: AssemblyTrademark("")]
[assembly: ComVisible(false)]
[assembly: AssemblyInformationalVersion("4.2.0.0")]
[assembly: AssemblyVersion("4.4.1.2")]
