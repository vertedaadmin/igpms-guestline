﻿// Decompiled with JetBrains decompiler
// Type: InfoGenesis.PublishedInterfaces.Contracts.PMS_XMLContract.CCPostInfo
// Assembly: PMS_XMLContract, Version=4.4.1.2, Culture=neutral, PublicKeyToken=null
// MVID: 1A6B76FD-4336-4474-B1B4-3C18D74F5E0E
// Assembly location: C:\Users\Tony\Desktop\PMS XML\PMS_XMLContract.dll

using System;

namespace InfoGenesis.PublishedInterfaces.Contracts.PMS_XMLContract
{
  [Serializable]
  public class CCPostInfo
  {
    public string AccountNum;
    public string ExpireDate;
    public string CVV2;
    public string CardSwipeTrack1;
    public string CardSwipeTrack2;
    public string AuthCode;
    public string InvoiceNumber;

    public CCPostInfo Clone()
    {
      return (CCPostInfo) this.MemberwiseClone();
    }
  }
}
