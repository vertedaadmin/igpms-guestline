﻿// Decompiled with JetBrains decompiler
// Type: InfoGenesis.PublishedInterfaces.Contracts.PMS_XMLContract.MItem
// Assembly: PMS_XMLContract, Version=4.4.1.2, Culture=neutral, PublicKeyToken=null
// MVID: 1A6B76FD-4336-4474-B1B4-3C18D74F5E0E
// Assembly location: C:\Users\Tony\Desktop\PMS XML\PMS_XMLContract.dll

using System;

namespace InfoGenesis.PublishedInterfaces.Contracts.PMS_XMLContract
{
  [Serializable]
  public class MItem
  {
    public uint Id;
    public int Qty;
    public long ExtPrice;
    public string ExtDiscount;
    public int OrderedProfitCenterId;
    public int OrderedMealPeriodId;

    public MItem()
    {
    }

    public MItem(uint Id, int Qty, long ExtPrice)
    {
      this.Id = Id;
      this.Qty = Qty;
      this.ExtPrice = ExtPrice;
    }
  }
}
