﻿// Decompiled with JetBrains decompiler
// Type: InfoGenesis.PublishedInterfaces.Contracts.PMS_XMLContract.AuthRequest
// Assembly: PMS_XMLContract, Version=4.4.1.2, Culture=neutral, PublicKeyToken=null
// MVID: 1A6B76FD-4336-4474-B1B4-3C18D74F5E0E
// Assembly location: C:\Users\Tony\Desktop\PMS XML\PMS_XMLContract.dll

using System;
using System.Xml.Serialization;

namespace InfoGenesis.PublishedInterfaces.Contracts.PMS_XMLContract
{
  [XmlRoot("Request")]
  [Serializable]
  public class AuthRequest
  {
    public string SecurityId;
    public string SecurityPassword;
    public string RoutingId;
    public string SourcePropertyId;
    public string DestinationPropertyId;
    public bool TrainingModeFlag;
    public uint RegisterId;
    public uint EmployeeId;
    public uint ProfitCenterId;
    public uint CheckNumber;
    public uint CheckTypeId;
    public uint MealPeriodId;
    public uint PostToPropertyId;
    public uint PaymentMethodId;
    public uint CoverCount;
    public long AuthAmount;
    public long TipAmount;
    public string AuthAccountNum;
    public string ExtraData;
    public string CardSwipeTrack1;
    public string CardSwipeTrack2;
    public BinCollection FinancialBinDetail;
  }
}
