﻿// Decompiled with JetBrains decompiler
// Type: InfoGenesis.PublishedInterfaces.Contracts.PMS_XMLContract.PostRequest
// Assembly: PMS_XMLContract, Version=4.4.1.2, Culture=neutral, PublicKeyToken=null
// MVID: 1A6B76FD-4336-4474-B1B4-3C18D74F5E0E
// Assembly location: C:\Users\Tony\Desktop\PMS XML\PMS_XMLContract.dll

using System;
using System.Xml.Serialization;

namespace InfoGenesis.PublishedInterfaces.Contracts.PMS_XMLContract
{
  [XmlRoot("Request")]
  [Serializable]
  public class PostRequest
  {
    public string SecurityId;
    public string SecurityPassword;
    public string RoutingId;
    public string UniquePostingId;
    public string SourcePropertyId;
    public string DestinationPropertyId;
    public uint ClientID;
    public uint PostingID;
    public string CloseTime;
    public bool RefundFlag;
    public uint AssociatedCheckNumber;
    public uint RegisterId;
    public uint CashierEmpId;
    public uint ServerEmpId;
    public uint ProfitCenterId;
    public uint CheckNumber;
    public uint CheckTypeId;
    public uint MealPeriodId;
    public uint PostToPropertyId;
    public uint PaymentMethodId;
    public uint PaymentSplitId;
    public uint CoverCount;
    public string TagData;
    public string ReceiptTextImage;
    public AccntPostInfo AccountDetail;
    public CCPostInfo CreditCardDetail;
    public long PaymentAmount;
    public long ChangeAmount;
    public long BreakageAmount;
    public long TipAmount;
    public BinCollection FinancialBinDetail;
    [XmlArrayItem("MenuItem")]
    public ItemCollection MenuItemList;
    [XmlArrayItem("Employee")]
    public EmpGratCollecion EmployeeGratTip;

    public PostRequest Clone()
    {
      return (PostRequest) this.MemberwiseClone();
    }
  }
}
