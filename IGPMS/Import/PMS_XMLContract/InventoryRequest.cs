﻿// Decompiled with JetBrains decompiler
// Type: InfoGenesis.PublishedInterfaces.Contracts.PMS_XMLContract.InventoryRequest
// Assembly: PMS_XMLContract, Version=4.4.1.2, Culture=neutral, PublicKeyToken=null
// MVID: 1A6B76FD-4336-4474-B1B4-3C18D74F5E0E
// Assembly location: C:\Users\Tony\Desktop\PMS XML\PMS_XMLContract.dll

using System;
using System.Xml.Serialization;

namespace InfoGenesis.PublishedInterfaces.Contracts.PMS_XMLContract
{
  [XmlRoot("Request")]
  [Serializable]
  public class InventoryRequest
  {
    public string SecurityId;
    public string SecurityPassword;
    public string RoutingId;
    public string UniquePostingId;
    public string SourcePropertyId;
    public string DestinationPropertyId;
    public uint ClientID;
    public uint PostingID;
    public string CloseTime;
    public string OpenTime;
    public bool RefundFlag;
    public uint AssociatedCheckNumber;
    public uint RegisterId;
    public uint CashierEmpId;
    public uint ServerEmpId;
    public uint ProfitCenterId;
    public uint CheckNumber;
    public uint CheckTypeId;
    public uint MealPeriodId;
    public uint CoverCount;
    public string TableId;
    public string TagData;
    public string ReceiptTextImage;
    public long TotalAmount;
    public long TipAmount;
    public PayCollection PaymentList;
    public BinCollection FinancialBinDetail;
    [XmlArrayItem("MenuItem")]
    public ItemCollection MenuItemList;
  }
}
