﻿// Decompiled with JetBrains decompiler
// Type: InfoGenesis.PublishedInterfaces.Contracts.PMS_XMLContract.BinCollection
// Assembly: PMS_XMLContract, Version=4.4.1.2, Culture=neutral, PublicKeyToken=null
// MVID: 1A6B76FD-4336-4474-B1B4-3C18D74F5E0E
// Assembly location: C:\Users\Tony\Desktop\PMS XML\PMS_XMLContract.dll

using InfoGenesis.PublishedInterfaces.Contracts.PMS_FinancialInfo;
using System;

namespace InfoGenesis.PublishedInterfaces.Contracts.PMS_XMLContract
{
  [Serializable]
  public class BinCollection : FinancialCollection
  {
    public FinancialBin this[int i]
    {
      get
      {
        return (FinancialBin) this.InnerList[i];
      }
      set
      {
        this.InnerList[i] = (object) value;
      }
    }

    public void Add(FinancialBin aBin)
    {
      this.InnerList.Add((object) aBin);
    }

    public override FinancialCollection NewCollection()
    {
      return (FinancialCollection) new BinCollection();
    }

    public override FinancialInfo NewFinancialInfo()
    {
      return (FinancialInfo) new FinancialBin();
    }

    public override FinancialInfo NewFinancialInfo(FinancialClass cID, int tID, long Amt)
    {
      return (FinancialInfo) new FinancialBin(cID, tID, Amt);
    }
  }
}
