﻿// Decompiled with JetBrains decompiler
// Type: InfoGenesis.PublishedInterfaces.Contracts.PMS_XMLContract.AcntCollection
// Assembly: PMS_XMLContract, Version=4.4.1.2, Culture=neutral, PublicKeyToken=null
// MVID: 1A6B76FD-4336-4474-B1B4-3C18D74F5E0E
// Assembly location: C:\Users\Tony\Desktop\PMS XML\PMS_XMLContract.dll

using System;
using System.Collections;

namespace InfoGenesis.PublishedInterfaces.Contracts.PMS_XMLContract
{
  [Serializable]
  public class AcntCollection : CollectionBase
  {
    public AccountDetail this[int i]
    {
      get
      {
        return (AccountDetail) this.InnerList[i];
      }
    }

    public void Add(AccountDetail aAcnt)
    {
      this.InnerList.Add((object) aAcnt);
    }
  }
}
