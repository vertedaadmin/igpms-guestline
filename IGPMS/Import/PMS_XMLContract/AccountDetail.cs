﻿// Decompiled with JetBrains decompiler
// Type: InfoGenesis.PublishedInterfaces.Contracts.PMS_XMLContract.AccountDetail
// Assembly: PMS_XMLContract, Version=4.4.1.2, Culture=neutral, PublicKeyToken=null
// MVID: 1A6B76FD-4336-4474-B1B4-3C18D74F5E0E
// Assembly location: C:\Users\Tony\Desktop\PMS XML\PMS_XMLContract.dll

using System;
using System.Xml.Serialization;

namespace InfoGenesis.PublishedInterfaces.Contracts.PMS_XMLContract
{
  [Serializable]
  public class AccountDetail
  {
    public string PostAccountNum;
    public string RoomNum;
    public string Name;
    public string AccountType;
    public string Message;
    public string URL;
    public MemberInfo Member;
    public long BalanceAmount;
    public bool VipFlag;
    public bool ChargingAllowedFlag;
    public uint PriceLevel;
    public GuestInfo GuestDetail;
    [XmlArrayItem("DiscountId")]
    public uint[] Discounts;
  }
}
