﻿// Decompiled with JetBrains decompiler
// Type: InfoGenesis.PublishedInterfaces.Contracts.PMS_XMLContract.EmpGrat
// Assembly: PMS_XMLContract, Version=4.4.1.2, Culture=neutral, PublicKeyToken=null
// MVID: 1A6B76FD-4336-4474-B1B4-3C18D74F5E0E
// Assembly location: C:\Users\Tony\Desktop\PMS XML\PMS_XMLContract.dll

using System;
using System.Xml.Serialization;

namespace InfoGenesis.PublishedInterfaces.Contracts.PMS_XMLContract
{
  [Serializable]
  public class EmpGrat
  {
    [XmlAttribute]
    public int Id;
    [XmlAttribute]
    public long Amt;

    public EmpGrat()
    {
    }

    public EmpGrat(int Id, long Amt)
    {
      this.Id = Id;
      this.Amt = Amt;
    }
  }
}
