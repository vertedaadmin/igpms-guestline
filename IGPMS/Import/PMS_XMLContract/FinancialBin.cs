﻿// Decompiled with JetBrains decompiler
// Type: InfoGenesis.PublishedInterfaces.Contracts.PMS_XMLContract.FinancialBin
// Assembly: PMS_XMLContract, Version=4.4.1.2, Culture=neutral, PublicKeyToken=null
// MVID: 1A6B76FD-4336-4474-B1B4-3C18D74F5E0E
// Assembly location: C:\Users\Tony\Desktop\PMS XML\PMS_XMLContract.dll

using InfoGenesis.PublishedInterfaces.Contracts.PMS_FinancialInfo;
using System;

namespace InfoGenesis.PublishedInterfaces.Contracts.PMS_XMLContract
{
  [Serializable]
  public class FinancialBin : FinancialInfo
  {
    public FinancialClass ClassId
    {
      get
      {
        return this.BinClass;
      }
      set
      {
        this.BinClass = value;
      }
    }

    public int TypeId
    {
      get
      {
        return this.BinID;
      }
      set
      {
        this.BinID = value;
      }
    }

    public long Amount
    {
      get
      {
        return this.BinAmt;
      }
      set
      {
        this.BinAmt = value;
      }
    }

    public FinancialBin()
    {
    }

    public FinancialBin(FinancialClass cID, int tID, long Amt)
    {
      this.BinClass = cID;
      this.BinID = tID;
      this.BinAmt = Amt;
    }

    public FinancialBin(FinancialClass cID, uint tID, long Amt)
      : this(cID, (int) tID, Amt)
    {
    }
  }
}
