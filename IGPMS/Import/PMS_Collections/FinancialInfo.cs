﻿// Decompiled with JetBrains decompiler
// Type: InfoGenesis.PublishedInterfaces.Contracts.PMS_FinancialInfo.FinancialInfo
// Assembly: PMS_Collections, Version=4.3.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C2091B65-369E-49AE-94F6-1E3AA9BCADA1
// Assembly location: C:\Users\Tony\Desktop\PMS XML\PMS_Collections.dll

using System;
using System.Xml.Serialization;

namespace InfoGenesis.PublishedInterfaces.Contracts.PMS_FinancialInfo
{
  [Serializable]
  public abstract class FinancialInfo
  {
    protected FinancialClass BinClass;
    protected int BinID;
    protected long BinAmt;

    [XmlIgnore]
    public FinancialClass FinClass
    {
      get
      {
        return this.BinClass;
      }
      set
      {
        this.BinClass = value;
      }
    }

    [XmlIgnore]
    public int FinID
    {
      get
      {
        return this.BinID;
      }
      set
      {
        this.BinID = value;
      }
    }

    [XmlIgnore]
    public long FinAmt
    {
      get
      {
        return this.BinAmt;
      }
      set
      {
        this.BinAmt = value;
      }
    }
  }
}
