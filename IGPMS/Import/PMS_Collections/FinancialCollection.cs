﻿// Decompiled with JetBrains decompiler
// Type: InfoGenesis.PublishedInterfaces.Contracts.PMS_FinancialInfo.FinancialCollection
// Assembly: PMS_Collections, Version=4.3.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C2091B65-369E-49AE-94F6-1E3AA9BCADA1
// Assembly location: C:\Users\Tony\Desktop\PMS XML\PMS_Collections.dll

using System;
using System.Collections;

namespace InfoGenesis.PublishedInterfaces.Contracts.PMS_FinancialInfo
{
  [Serializable]
  public abstract class FinancialCollection : CollectionBase
  {
    public FinancialInfo this[int i]
    {
      get
      {
        return (FinancialInfo) this.InnerList[i];
      }
      set
      {
        this.InnerList[i] = (object) value;
      }
    }

    public void Add(FinancialInfo aBin)
    {
      this.InnerList.Add((object) aBin);
    }

    public abstract FinancialCollection NewCollection();

    public abstract FinancialInfo NewFinancialInfo();

    public abstract FinancialInfo NewFinancialInfo(FinancialClass cID, int tID, long Amt);
  }
}
