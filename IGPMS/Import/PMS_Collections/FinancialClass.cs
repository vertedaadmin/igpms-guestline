﻿// Decompiled with JetBrains decompiler
// Type: InfoGenesis.PublishedInterfaces.Contracts.PMS_FinancialInfo.FinancialClass
// Assembly: PMS_Collections, Version=4.3.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C2091B65-369E-49AE-94F6-1E3AA9BCADA1
// Assembly location: C:\Users\Tony\Desktop\PMS XML\PMS_Collections.dll

using System;

namespace InfoGenesis.PublishedInterfaces.Contracts.PMS_FinancialInfo
{
  [Serializable]
  public enum FinancialClass
  {
    Sales = 1,
    Discount = 2,
    Tax = 3,
    Gratuity = 4,
    ServiceCharge = 5,
    Tip = 6,
    Round = 7,
  }
}
