﻿using System.Reflection;

[assembly: AssemblyCompany("InfoGenesis")]
[assembly: AssemblyProduct("InfoGenesis POS")]
[assembly: AssemblyCopyright("© 2005 - 2011 InfoGenesis")]
[assembly: AssemblyDescription("PMS Financial (& Other) Data Collections")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyInformationalVersion("4.1.6.464")]
[assembly: AssemblyTitle("PMS_Collections Class Library")]
[assembly: AssemblyDelaySign(false)]
[assembly: AssemblyKeyName("")]
[assembly: AssemblyVersion("4.3.0.0")]
