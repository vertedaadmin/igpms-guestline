﻿namespace IGPMS
{
    using Bugsnag;
    using Bugsnag.Clients;

    using InfoGenesis.PublishedInterfaces.Contracts.PMS_XMLContract;
    using MenuSync;
    using IGPMS3.net.rezlynx.muk;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Web;
    using System.Web.Services;
    using System.Xml;
    using System.Xml.Serialization;

    [WebService(Namespace = "PMSI"), WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1), ToolboxItem(false)]
    public class IGPMSWebService : WebService
    {
        public BaseClient bugsnag = new BaseClient("e971d4bc9ee3f926c53adab068835323");



        public IGPMSWebService()
        {
            
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;


            string str = HttpContext.Current.Server.MapPath("~/");
            try
            {
                string logLocation = str + @"IGPMSLog\";
                this.TEST_Write_Config(DateTime.Now + " : The config path is: " + logLocation);
                if (File.Exists(logLocation + "IGPMS.log"))
                {
                    VertedaDiagnostics.OpenLog(logLocation, "IGPMS.log");
                }
                else
                {
                    File.Create(logLocation + "IGPMS.log").Dispose();
                    VertedaDiagnostics.OpenLog(logLocation, "IGPMS.log");
                }
                VertedaDiagnostics.LogMessage("IGPMS WebService Reporting Started", "I", false);
                VertedaDiagnostics.LogMessage("IGPMS WebService path = " + str, "I", false);
                this.TEST_Write_Config(DateTime.Now + ": IGPMS WebService Reporting Started");
            }
            catch
            {
            }
        }

        [WebMethod(Description = "Authorize charging to a specific account number")]
        public AuthResponse AccountAuthorizationVer1(AuthRequest Request)
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;


            AuthResponse response = new AuthResponse
            {
                PostAccountNum = Request.AuthAccountNum
            };
            try
            {
                VertedaDiagnostics.LogMessage("AccountAuthorizationVer1 started.", "I", false);
                this.TEST_Write_Config(DateTime.Now + " AccountAuthorizationVer1 started.");
                StringWriter writer = new StringWriter();
                new XmlSerializer(Request.GetType()).Serialize((TextWriter)writer, Request);
                object[] objArray1 = new object[] { DateTime.Now, " AccountLookupVer1: Input Request is: ", Environment.NewLine, writer.ToString() };
                this.TEST_Write_Config(string.Concat(objArray1));
                VertedaDiagnostics.LogMessage("AccountAuthorizationVer1 finished.", "I", false);
                this.TEST_Write_Config(DateTime.Now + " AccountAuthorizationVer1 finished.");
            }
            catch (Exception exception)
            {
                VertedaDiagnostics.LogMessage("Error in AccountAuthorizationVer1. " + exception.ToString(), "I", false);
                this.TEST_Write_Config(DateTime.Now + "Error in  AccountAuthorizationVer1. " + exception.ToString());
            }
            return response;
        }

        [WebMethod(Description = "Obtain a list of account numbers associated with a search criteria")]
        public LookupResponse AccountLookupVer1(LookupRequest Request)
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            LookupResponse response = new LookupResponse();
            try
            {
                VertedaDiagnostics.LogMessage("AccountLookupVer1 started.", "I", false);
                this.TEST_Write_Config(DateTime.Now + " AccountLookupVer1 started.");
                StringWriter writer = new StringWriter();
                new XmlSerializer(Request.GetType()).Serialize((TextWriter)writer, Request);
                object[] objArray1 = new object[] { DateTime.Now, " AccountLookupVer1: Input Request is: ", Environment.NewLine, writer.ToString() };
                this.TEST_Write_Config(string.Concat(objArray1));
                response = this.IG_Account_Lookup_To_PMS(Request);
                VertedaDiagnostics.LogMessage("AccountLookupVer1 finished.", "I", false);
                this.TEST_Write_Config(DateTime.Now + " AccountLookupVer1 finished.");
            }
            catch (Exception exception)
            {
                VertedaDiagnostics.LogMessage("Error in AccountLookupVer1. " + exception.ToString(), "I", false);
                this.TEST_Write_Config(DateTime.Now + "Error in  AccountLookupVer1. " + exception.ToString());
            }
            return response;
        }

        [WebMethod(Description = "Authorize a charge to a standard credit card")]
        public CCAuthResponse CreditCardAuthVer1(CCAuthRequest Request)
        {
            CCAuthResponse response = new CCAuthResponse();
            VertedaDiagnostics.LogMessage("CreditCardAuthVer1 started.", "I", false);
            this.TEST_Write_Config(DateTime.Now + " CreditCardAuthVer1 started.");
            StringWriter writer = new StringWriter();
            new XmlSerializer(Request.GetType()).Serialize((TextWriter)writer, Request);
            object[] objArray1 = new object[] { DateTime.Now, " CreditCardAuthVer1: Input Request is: ", Environment.NewLine, writer.ToString() };
            this.TEST_Write_Config(string.Concat(objArray1));
            return response;
        }

        [WebMethod(Description = "Indicate reachability of the Web Service")]
        public StatusResponse GetPmsStatusVer1(StatusRequest Request)
        {
            StatusResponse response = new StatusResponse();
            try
            {
                VertedaDiagnostics.LogMessage("GetPmsStatusVer1 started.", "I", false);
                this.TEST_Write_Config(DateTime.Now + " GetPmsStatusVer1 started.");
                ConfigSettings conf = this.ReadConfig();
                if (!conf.Error)
                {
                    new RezLynxWebServiceRouter().Url = conf.Url;
                    PMSLogInResponse response2 = this.PMS_Login(conf);
                    response.StatusValue = Convert.ToInt32(response2.status);
                    response.StatusMessage = response2.errorText;
                }
            }
            catch (Exception)
            {
            }
            return response;
        }

        public LookupResponse IG_Account_Lookup_To_PMS(LookupRequest lookup)
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;


            LookupResponse o = new LookupResponse();
            try
            {
                object[] objArray1 = new object[] { DateTime.Now, "IG_Account_Lookup_To_PMS started: input ", lookup.InputData, ", input key & flag: ", lookup.MoreRecordsFlag.ToString(), ", ", lookup.MoreRecordsKey.ToString() };
                this.TEST_Write_Config(string.Concat(objArray1));
                int num = 0;
                try
                {
                    if (lookup.MoreRecordsFlag && (lookup.MoreRecordsKey != null))
                    {
                        num = Convert.ToInt32(lookup.MoreRecordsKey);
                    }
                }
                catch (Exception)
                {
                }
                VertedaDiagnostics.LogMessage("IG_Account_Lookup_To_PMS started.", "I", false);
                this.TEST_Write_Config(DateTime.Now + " IG_Account_Lookup_To_PMS started.");
                ConfigSettings conf = this.ReadConfig();
                string message = this.Test_ReadConfig();
                VertedaDiagnostics.LogMessage(message, "I", false);
                this.TEST_Write_Config(DateTime.Now + "the config details are: " + message);
                if (conf.Error)
                {
                    o.ErrorDebugMessage = "Error in IG_Account_Lookup_To_PMS: Could not get config information";
                    VertedaDiagnostics.LogMessage("Error in IG_Account_Lookup_To_PMS: Could not get config information", "E", false);
                    this.TEST_Write_Config(DateTime.Now + " : Error in IG_Account_Lookup_To_PMS: Could not get config information");
                }
                else
                {
                    RezLynxWebServiceRouter router = new RezLynxWebServiceRouter
                    {
                        Url = conf.Url
                    };
                    PMSLogInResponse response2 = this.PMS_Login(conf);
                    if (!response2.status)
                    {
                        o.ErrorDebugMessage = "Error in IG_Account_Lookup_To_PMS: could not log in";
                        VertedaDiagnostics.LogMessage("Error in IG_Account_Lookup_To_PMS: could not log in", "E", false);
                        this.TEST_Write_Config(DateTime.Now + " : Error in IG_Account_Lookup_To_PMS: could not log in");
                    }
                    else
                    {
                        string sessionID = response2.Session_id;
                        ReservationBookingSearchFilters filters = new ReservationBookingSearchFilters();
                        ReservationBookingSearchResults searchResults = new ReservationBookingSearchResults();
                        if (lookup.ByNameFlag)
                        {
                            filters.Surname = lookup.InputData;
                        }
                        else
                        {
                            filters.RoomID = lookup.InputData;
                        }
                        filters.Limit = 200;
                        filters.ReturnAllGuestsInNameSearches = true;
                        filters.PreCheckIn = false;
                        filters.BookingStatus = new BookingSearchBookingStatus[] { BookingSearchBookingStatus.Resident };
                        cResponse response3 = router.pmsbkg_BookingSearch(sessionID, filters, ref searchResults);
                        if (response3.ExceptionCode == 0)
                        {
                            VertedaDiagnostics.LogMessage("IG_Account_Lookup_To_PMS booking search completed. " + searchResults.Reservations.Count<ReservationBookingSearchItem>() + "matching accouts found", "I", false);
                            object[] objArray2 = new object[] { DateTime.Now, " IG_Account_Lookup_To_PMS booking search completed. ", searchResults.Reservations.Count<ReservationBookingSearchItem>(), "matching accouts found" };
                            this.TEST_Write_Config(string.Concat(objArray2));
                            StringWriter writer = new StringWriter();
                            new XmlSerializer(searchResults.GetType()).Serialize((TextWriter)writer, searchResults);
                            object[] objArray3 = new object[] { DateTime.Now, " IG_Account_Lookup_To_PMS: Results from Guestline are: ", Environment.NewLine, writer.ToString() };
                            this.TEST_Write_Config(string.Concat(objArray3));
                            if (searchResults.Reservations.Count<ReservationBookingSearchItem>() == 0)
                            {
                                this.TEST_Write_Config(DateTime.Now + "IG_Account_Lookup_To_PMS: No records found");
                                o.ErrorCode = response3.ExceptionCode;
                                o.ErrorDisplayMessage = "No records found";
                                o.ErrorDebugMessage = response3.ExceptionDescription;
                                o.MoreRecordsFlag = false;
                                o.MoreRecordsKey = "";
                            }
                            else if (searchResults.Reservations.Count<ReservationBookingSearchItem>() == 1)
                            {
                                this.TEST_Write_Config(DateTime.Now + "IG_Account_Lookup_To_PMS: One record found");
                                o.ErrorCode = response3.ExceptionCode;
                                o.MoreRecordsFlag = false;
                                AcntCollection acnts = new AcntCollection();
                                List<AccountDetail> list1 = new List<AccountDetail>();
                                AccountDetail detail = new AccountDetail
                                {
                                    PostAccountNum = searchResults.Reservations[0].Contact.ProfileRef,
                                    RoomNum = searchResults.Reservations[0].RoomId,
                                    Name = searchResults.Reservations[0].Contact.Name,
                                    VipFlag = false,
                                    ChargingAllowedFlag = true
                                };
                                GuestInfo info1 = new GuestInfo();
                                acnts.Add(detail);
                                o.MatchingAccounts = acnts;
                                StringWriter writer2 = new StringWriter();
                                new XmlSerializer(o.GetType()).Serialize((TextWriter)writer2, o);
                                object[] objArray4 = new object[] { DateTime.Now, " IG_Account_Lookup_To_PMS: Converted response to send back to IG is: ", Environment.NewLine, writer2.ToString() };
                                this.TEST_Write_Config(string.Concat(objArray4));
                            }
                            else if (searchResults.Reservations.Count<ReservationBookingSearchItem>() > 1)
                            {
                                this.TEST_Write_Config(DateTime.Now + "IG_Account_Lookup_To_PMS: Multiple records found");
                                o.ErrorCode = response3.ExceptionCode;
                                o.ErrorDisplayMessage = "More than one record found";
                                o.ErrorDebugMessage = response3.ExceptionDescription;
                                o.MoreRecordsFlag = true;
                                AcntCollection acnts2 = new AcntCollection();
                                List<AccountDetail> list2 = new List<AccountDetail>();
                                this.TEST_Write_Config(DateTime.Now + "IG_Account_Lookup_To_PMS: point 1. Results count = " + searchResults.Reservations.Count<ReservationBookingSearchItem>());
                                int index = num;
                                while (true)
                                {
                                    string profileRef;
                                    string name;
                                    if ((index >= searchResults.Reservations.Count<ReservationBookingSearchItem>()) || (index >= (num + 10)))
                                    {
                                        o.MatchingAccounts = acnts2;
                                        this.TEST_Write_Config(DateTime.Now + "IG_Account_Lookup_To_PMS: point 2");
                                        if (lookup.MoreRecordsFlag)
                                        {
                                            this.TEST_Write_Config(DateTime.Now + "IG_Account_Lookup_To_PMS: point 3");
                                            num = Convert.ToInt32(num) + 10;
                                        }
                                        else
                                        {
                                            this.TEST_Write_Config(DateTime.Now + "IG_Account_Lookup_To_PMS: point 4");
                                            num = 10;
                                        }
                                        o.MoreRecordsKey = num.ToString();
                                        this.TEST_Write_Config(DateTime.Now + "IG_Account_Lookup_To_PMS: point 5");
                                        StringWriter writer3 = new StringWriter();
                                        new XmlSerializer(o.GetType()).Serialize((TextWriter)writer3, o);
                                        object[] objArray5 = new object[] { DateTime.Now, " IG_Account_Lookup_To_PMS: Converted response to send back to IG is: ", Environment.NewLine, writer3.ToString() };
                                        this.TEST_Write_Config(string.Concat(objArray5));
                                        break;
                                    }
                                    AccountDetail detail2 = new AccountDetail();
                                    ReservationBookingSearchProfile contact = searchResults.Reservations[index].Contact;
                                    if (contact != null)
                                    {
                                        profileRef = contact.ProfileRef;
                                    }
                                    else
                                    {
                                        ReservationBookingSearchProfile local2 = contact;
                                        profileRef = null;
                                    }
                                    string local3 = profileRef;
                                    detail2.PostAccountNum = local3 ?? "Null";
                                    string roomId = searchResults.Reservations[index].RoomId;
                                    detail2.RoomNum = roomId ?? "0";
                                    ReservationBookingSearchProfile profile2 = searchResults.Reservations[index].Contact;
                                    if (profile2 != null)
                                    {
                                        name = profile2.Name;
                                    }
                                    else
                                    {
                                        ReservationBookingSearchProfile local6 = profile2;
                                        name = null;
                                    }
                                    string local7 = name;
                                    profile2.Name = local7 ?? "Null";
                                    detail2.VipFlag = false;
                                    detail2.ChargingAllowedFlag = true;
                                    GuestInfo info2 = new GuestInfo();
                                    acnts2.Add(detail2);
                                    index++;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                o.ErrorDebugMessage = "Error in IG_Account_Lookup_To_PMS: Could not get config information";
                this.IGPMS_SendBugsnag("Error in IG_Account_Lookup_To_PMS", exception, "Database");
                VertedaDiagnostics.LogMessage("Error in IG_Account_Lookup_To_PMS: " + exception.ToString(), "E", false);
                this.TEST_Write_Config(DateTime.Now + " : Error in IG_Account_Lookup_To_PMS: " + exception.ToString());
            }
            return o;
        }

        public PostResponse IG_Payment_Post_To_PMS(PostRequest request)
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;


            PostResponse response = new PostResponse();
            try
            {
                VertedaDiagnostics.LogMessage("IG_Payment_Post_To_PMS started.", "I", false);
                this.TEST_Write_Config(DateTime.Now + " IG_Payment_Post_To_PMS started.");
                ConfigSettings conf = this.ReadConfig();
                RevenueMappings mappings = this.ReadMappings();
                if (conf.Error || mappings.Error)
                {
                    VertedaDiagnostics.LogMessage("Error in IG_Payment_Post_To_PMS: Could not get config information", "E", false);
                    this.TEST_Write_Config(DateTime.Now + " : Error in IG_Payment_Post_To_PMS: Could not get config information");
                }
                else
                {
                    RezLynxWebServiceRouter router = new RezLynxWebServiceRouter
                    {
                        Url = conf.Url
                    };
                    PMSLogInResponse response2 = this.PMS_Login(conf);
                    if (!response2.status)
                    {
                        VertedaDiagnostics.LogMessage("Error in IG_Payment_Post_To_PMS: Could not get login information", "E", false);
                        this.TEST_Write_Config(DateTime.Now + " : Error in IG_Payment_Post_To_PMS: Could not get login information");
                    }
                    else
                    {
                        int num = 0;
                        List<FinancialBin> list = new List<FinancialBin>();
                        this.TEST_Write_Config(DateTime.Now + " point 0.");
                        foreach (FinancialBin bin in request.FinancialBinDetail)
                        {
                            object[] objArray1 = new object[] { DateTime.Now, " point 0.5 amount / classid", bin.Amount, " ", bin.ClassId };
                            this.TEST_Write_Config(string.Concat(objArray1));
                            if ((bin.Amount != 0) && ((int)bin.ClassId != 3))
                            {
                                num++;
                                list.Add(bin);
                            }
                        }
                        string sessionID = response2.Session_id;
                        this.TEST_Write_Config(DateTime.Now + " point 1. session ID: " + sessionID);
                        if (request.TipAmount > 0L)
                        {
                            num++;
                        }
                        epmsbkg_RoomFreePostingMethod postingMethod = epmsbkg_RoomFreePostingMethod.PostingMethod_RoomID;
                        string postingParameter = "";
                        int folioID = 0;
                        FinancialTransactionHeader o = new FinancialTransactionHeader();
                        FinancialTransaction[] transactionArray = new FinancialTransaction[num];
                        postingMethod = epmsbkg_RoomFreePostingMethod.PostingMethod_RoomID;
                        postingParameter = request.AccountDetail.RoomNum;
                        o.Description = "EPOS Postings: " + request.CheckNumber.ToString() + ", food & beverage: " + request.RegisterId.ToString();
                        this.TEST_Write_Config(DateTime.Now + " point 1.5. pre defaultRevenue: ");
                        string str3 = (from x in mappings.RevenueCodes
                                       where (x.TypeID == 0) && ((x.ProfitCenter == 0) && ((x.MealID == 0) && (x.ClassID == "Default")))
                                       select x.AnalCode).FirstOrDefault<string>().ToString();
                        this.TEST_Write_Config(DateTime.Now + " point 2. defaultRevenue: " + str3);
                        int index = 0;
                        using (List<FinancialBin>.Enumerator enumerator2 = list.GetEnumerator())
                        {
                            while (enumerator2.MoveNext())
                            {
                                FinancialBin fb = enumerator2.Current;
                                object[] objArray2 = new object[13];
                                objArray2[0] = DateTime.Now;
                                objArray2[1] = " point 3. fb: ";
                                objArray2[2] = fb.Amount;
                                objArray2[3] = " ";
                                objArray2[4] = fb.ClassId;
                                objArray2[5] = " ";
                                objArray2[6] = fb.FinAmt;
                                objArray2[7] = " ";
                                objArray2[8] = fb.FinClass;
                                objArray2[9] = " ";
                                objArray2[10] = fb.FinID;
                                objArray2[11] = " ";
                                objArray2[12] = fb.TypeId;
                                this.TEST_Write_Config(string.Concat(objArray2));
                                FinancialTransaction transaction = new FinancialTransaction();
                                string str4 = str3;
                                if ((((int)fb.ClassId == 1) || ((int)fb.ClassId == 2)) || ((int)fb.ClassId == 5))
                                {
                                    string str5 = (from x in mappings.RevenueCodes.Where<RevenueToAnalysis>(delegate (RevenueToAnalysis x) {
                                        if (((x.TypeID != fb.TypeId) || (x.ProfitCenter != request.ProfitCenterId)) || (x.MealID != request.MealPeriodId))
                                        {
                                            return false;
                                        }
                                        return x.ClassID == fb.ClassId.ToString();
                                    })
                                                   select x.AnalCode).FirstOrDefault<string>();
                                    if (str5 != null)
                                    {
                                        str4 = str5.ToString();
                                    }
                                    transaction.AnalysisCode = str4;
                                }
                                else if ((int)fb.ClassId == 4)
                                {
                                    string str6 = (from x in mappings.RevenueCodes.Where<RevenueToAnalysis>(delegate (RevenueToAnalysis x) {
                                        if ((x.TypeID != fb.TypeId) || (x.ProfitCenter != request.ProfitCenterId))
                                        {
                                            return false;
                                        }
                                        return x.ClassID == fb.ClassId.ToString();
                                    })
                                                   select x.AnalCode).FirstOrDefault<string>();
                                    if (str6 != null)
                                    {
                                        str4 = str6.ToString();
                                    }
                                    transaction.AnalysisCode = str4;
                                }
                                transaction.Description = fb.ClassId.ToString();
                                transaction.GrossTotal = fb.Amount / 100.00M;
                                transaction.Quantity = 1;
                                transaction.ShiftNo = 0;
                                transactionArray[index] = transaction;
                                index++;
                            }
                        }
                        this.TEST_Write_Config(DateTime.Now + " point 4. fbs finished ");
                        if (request.TipAmount > 0L)
                        {
                            string str7 = str3;
                            FinancialTransaction transaction2 = new FinancialTransaction();
                            string str8 = (from x in mappings.RevenueCodes
                                           where (x.ProfitCenter == request.ProfitCenterId) && (x.ClassID == "Tips")
                                           select x.AnalCode).FirstOrDefault<string>();
                            if (str8 != null)
                            {
                                str7 = str8.ToString();
                            }
                            transaction2.AnalysisCode = str7;
                            transaction2.Description = "Tips";
                            transaction2.GrossTotal = request.TipAmount / 100.00M;
                            transaction2.Quantity = 1;
                            transaction2.ShiftNo = 0;
                            transactionArray[index] = transaction2;
                        }
                        this.TEST_Write_Config(DateTime.Now + " IG_Payment_Post_To_PMS: sessionID : " + sessionID.ToString());
                        this.TEST_Write_Config(DateTime.Now + " IG_Payment_Post_To_PMS: postingMethod : " + postingMethod.ToString());
                        this.TEST_Write_Config(DateTime.Now + " IG_Payment_Post_To_PMS: postingParam : " + postingParameter.ToString());
                        this.TEST_Write_Config(DateTime.Now + " IG_Payment_Post_To_PMS: folioID : " + folioID.ToString());
                        StringWriter writer = new StringWriter();
                        new XmlSerializer(o.GetType()).Serialize((TextWriter)writer, o);
                        object[] objArray3 = new object[] { DateTime.Now, " IG_Payment_Post_To_PMS: headers sent are : ", Environment.NewLine, writer.ToString() };
                        this.TEST_Write_Config(string.Concat(objArray3));
                        StringWriter writer2 = new StringWriter();
                        new XmlSerializer(transactionArray.GetType()).Serialize((TextWriter)writer2, transactionArray);
                        object[] objArray4 = new object[] { DateTime.Now, " IG_Payment_Post_To_PMS: transactions sent are : ", Environment.NewLine, writer2.ToString() };
                        this.TEST_Write_Config(string.Concat(objArray4));
                        cResponse response3 = router.pmschg_PostToRoom(sessionID, postingMethod, postingParameter, folioID, o, transactionArray);
                        response.PostingStatus = (response3.ExceptionCode != 0) ? ((StatusVal)4) : ((StatusVal)1);
                        response.PostingMessage = response3.ExceptionDescription;
                    }
                }
            }
            catch (Exception exception)
            {
                this.IGPMS_SendBugsnag("Error in IG_Payment_Post_To_PMS", exception, "Database");
                VertedaDiagnostics.LogMessage("Error in IG_Payment_Post_To_PMS: " + exception.ToString(), "E", false);
                this.TEST_Write_Config(DateTime.Now + " : Error in IG_Payment_Post_To_PMS: " + exception.ToString());
            }
            return response;
        }

        public void IGPMS_SendBugsnag(string issueDescription, Exception ex, string snagType)
        {
            try
            {
                IPAddress[] hostAddresses = Dns.GetHostAddresses(Dns.GetHostName());
                Metadata metadata = new Metadata();
                metadata.AddToTab("Additional Info", "Description", issueDescription);
                int index = 0;
                while (true)
                {
                    if (index >= hostAddresses.Length)
                    {
                        if (ex != null)
                        {
                            metadata.AddToTab("Additional Info", "Error message", ex.Message);
                            metadata.AddToTab("Additional Info", "Full Stack Trace", ex.StackTrace);
                        }
                        this.bugsnag.Config.SetUser("Verteda IGPMS", "", "Verteda IGPMS");
                        this.bugsnag.Config.AppVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();
                        this.bugsnag.Notify(new ArgumentException(snagType), metadata);
                        break;
                    }
                    if (!IPAddress.IsLoopback(hostAddresses[index]) && (hostAddresses[index].AddressFamily.ToString().IndexOf("V6") == -1))
                    {
                        metadata.AddToTab("Additional Info", "IP Address", hostAddresses[index].ToString());
                    }
                    index++;
                }
            }
            catch (Exception)
            {
            }
        }

        [WebMethod(Description = "Post a POS check transaction to an inventory system")]
        public InventoryResponse InventoryPostVer1(InventoryRequest Request)
        {
            InventoryResponse response = new InventoryResponse();
            VertedaDiagnostics.LogMessage("InventoryPostVer1 started.", "I", false);
            this.TEST_Write_Config(DateTime.Now + " InventoryPostVer1 started.");
            StringWriter writer = new StringWriter();
            new XmlSerializer(Request.GetType()).Serialize((TextWriter)writer, Request);
            object[] objArray1 = new object[] { DateTime.Now, " InventoryPostVer1: Input Request is: ", Environment.NewLine, writer.ToString() };
            this.TEST_Write_Config(string.Concat(objArray1));
            return response;
        }

        [WebMethod(Description = "Post a POS payment to the PMS Host")]
        public PostResponse PaymentPostVer1(PostRequest Request)
        {
            PostResponse response1 = new PostResponse();
            VertedaDiagnostics.LogMessage("PaymentPostVer1 started.", "I", false);
            this.TEST_Write_Config(DateTime.Now + " PaymentPostVer1 started.");
            StringWriter writer = new StringWriter();
            new XmlSerializer(Request.GetType()).Serialize((TextWriter)writer, Request);
            object[] objArray1 = new object[] { DateTime.Now, " PaymentPostVer1: Input Request is: ", Environment.NewLine, writer.ToString() };
            this.TEST_Write_Config(string.Concat(objArray1));
            return this.IG_Payment_Post_To_PMS(Request);
        }

        [WebMethod]
        public PMSLogInResponse PMS_Analysis_List(string sessionID)
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;


            PMSLogInResponse response = new PMSLogInResponse();
            try
            {
                VertedaDiagnostics.LogMessage("PMS_Analysis_List started", "I", false);
                this.TEST_Write_Config(DateTime.Now + " PMS_Analysis_List started");
                ConfigSettings settings = this.ReadConfig();
                if (settings.Error)
                {
                    response.status = false;
                    response.errorText = "Error in PMS_Analysis_List: Could not get config information";
                    VertedaDiagnostics.LogMessage("Error in PMS_Analysis_List: Could not get config information", "E", false);
                    this.TEST_Write_Config(DateTime.Now + " : Error in PMS_Analysis_List: Could not get config information");
                }
                else
                {
                    RezLynxWebServiceRouter router1 = new RezLynxWebServiceRouter();
                    router1.Url = settings.Url;
                    cpmscfg_AnalList analysisCodeList = new cpmscfg_AnalList();
                    router1.pmscfg_AnalList(sessionID, ref analysisCodeList);
                    StringWriter writer = new StringWriter();
                    new XmlSerializer(analysisCodeList.GetType()).Serialize((TextWriter)writer, analysisCodeList);
                    object[] objArray1 = new object[] { DateTime.Now, " PMS_Analysis_List: Results from Guestline are: ", Environment.NewLine, writer.ToString() };
                    this.TEST_Write_Config(string.Concat(objArray1));
                }
            }
            catch (Exception exception)
            {
                response.status = false;
                response.errorText = "Error in PMS_Analysis_List: " + exception.ToString();
                this.IGPMS_SendBugsnag("Error in PMS_Analysis_List", exception, "Database");
                VertedaDiagnostics.LogMessage("Error in PMS_Analysis_List: " + exception.ToString(), "E", false);
                this.TEST_Write_Config(DateTime.Now + " : Error in PMS_Analysis_List: " + exception.ToString());
            }
            return response;
        }

        [WebMethod]
        public PMSLogInResponse PMS_Login(ConfigSettings conf)
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            PMSLogInResponse response = new PMSLogInResponse();
            try
            {
                VertedaDiagnostics.LogMessage("PMS_Login started", "I", false);
                this.TEST_Write_Config(DateTime.Now + " PMS_Login started");
                if (conf.Error)
                {
                    response.status = false;
                    response.errorText = "Error in PMS_Login: Could not get config information";
                    VertedaDiagnostics.LogMessage("Error in PMS_Login: Could not get config information", "E", false);
                    this.TEST_Write_Config(DateTime.Now + " : Error in PMS_Login: Could not get config information");
                }
                else
                {
                    RezLynxWebServiceRouter router1 = new RezLynxWebServiceRouter();
                    router1.Url = conf.Url;
                    string sessionID = string.Empty;
                    cResponse response2 = router1.LogIn(conf.SiteID, conf.InterfaceID, conf.OperatorCode, conf.Password, ref sessionID);
                    if (string.IsNullOrEmpty(sessionID) || (response2.ExceptionCode != 0))
                    {
                        response.status = false;
                        response.errorText = $"Login Failure : {response2.ExceptionCode} : {response2.ExceptionDescription} ";
                        VertedaDiagnostics.LogMessage("PMS_Login: " + response.errorText, "I", false);
                        this.TEST_Write_Config(DateTime.Now + " : PMS_Login: " + response.errorText);
                        response.Session_id = "0";
                    }
                    else
                    {
                        response.status = true;
                        response.errorText = $"Login Success : {response2.ExceptionCode} : {response2.ExceptionDescription} ";
                        VertedaDiagnostics.LogMessage("PMS_Login: " + response.errorText, "I", false);
                        this.TEST_Write_Config(DateTime.Now + " : PMS_Login: " + response.errorText);
                        response.Session_id = sessionID;
                    }
                }
            }
            catch (Exception exception)
            {
                response.status = false;
                response.errorText = "Error in PMS_Login: " + exception.ToString();
                this.IGPMS_SendBugsnag("Error in PMS_Login", exception, "Database");
                VertedaDiagnostics.LogMessage("Error in PMS_Login: " + exception.ToString(), "E", false);
                this.TEST_Write_Config(DateTime.Now + " : Error in PMS_Login: " + exception.ToString());
            }
            return response;
        }

        private ConfigSettings ReadConfig()
        {
            ConfigSettings settings = new ConfigSettings();
            try
            {
                VertedaDiagnostics.LogMessage("ReadConfig started", "I", false);
                this.TEST_Write_Config(DateTime.Now + " : ReadConfig started");
                XmlDocument document = new XmlDocument();
                document.Load(HttpContext.Current.Server.MapPath("~/") + @"\IGPMSConfig\IGPMSConfig.xml");
                foreach (XmlNode node in document.SelectNodes("//SiteID"))
                {
                    settings.SiteID = node.InnerText;
                }
                foreach (XmlNode node2 in document.SelectNodes("//InterfaceID"))
                {
                    settings.InterfaceID = Convert.ToInt32(node2.InnerText);
                }
                foreach (XmlNode node3 in document.SelectNodes("//OperatorCode"))
                {
                    settings.OperatorCode = node3.InnerText;
                }
                foreach (XmlNode node4 in document.SelectNodes("//Password"))
                {
                    settings.Password = node4.InnerText;
                }
                foreach (XmlNode node5 in document.SelectNodes("//URL"))
                {
                    settings.Url = node5.InnerText;
                }
                settings.Error = false;
            }
            catch (Exception exception)
            {
                this.IGPMS_SendBugsnag("Error in ReadConfig", exception, "Database");
                VertedaDiagnostics.LogMessage("Error in ReadConfig. Couldn't read config: " + exception.ToString(), "E", false);
                this.TEST_Write_Config(DateTime.Now + " : Error in ReadConfig. Couldn't read config: " + exception.ToString());
                settings.Error = true;
            }
            return settings;
        }

        private RevenueMappings ReadMappings()
        {
            RevenueMappings mappings = new RevenueMappings();
            try
            {
                VertedaDiagnostics.LogMessage("ReadMappings started", "I", false);
                this.TEST_Write_Config(DateTime.Now + " : ReadMappings started");
                char[] separator = new char[] { '\n' };
                string[] source = File.ReadAllText(HttpContext.Current.Server.MapPath("~/") + @"\IGPMSConfig\IGPMSMappings.csv").Split(separator);
                List<RevenueToAnalysis> list = new List<RevenueToAnalysis>();
                int index = 1;
                while (true)
                {
                    if (index >= source.Count<string>())
                    {
                        mappings.RevenueCodes = list;
                        mappings.Error = false;
                        break;
                    }
                    if (!string.IsNullOrEmpty(source[index]))
                    {
                        char[] chArray2 = new char[] { ',' };
                        string[] strArray2 = source[index].Split(chArray2);
                        if (strArray2.Count<string>() != 5)
                        {
                            VertedaDiagnostics.LogMessage("Error in ReadMappings: Incorrect number of objects on line " + index, "E", false);
                            this.TEST_Write_Config(DateTime.Now + " : Error in ReadMappings: Incorrect number of objects on line " + index);
                        }
                        else
                        {
                            RevenueToAnalysis item = new RevenueToAnalysis
                            {
                                ClassID = strArray2[0],
                                TypeID = Convert.ToInt32(strArray2[1]),
                                ProfitCenter = Convert.ToInt32(strArray2[2]),
                                MealID = Convert.ToInt32(strArray2[3]),
                                AnalCode = strArray2[4].Replace("\r", "")
                            };
                            list.Add(item);
                        }
                    }
                    index++;
                }
            }
            catch (Exception exception)
            {
                this.IGPMS_SendBugsnag("Error in ReadMappings", exception, "Database");
                VertedaDiagnostics.LogMessage("Error in ReadMappings. Couldn't read config: " + exception.ToString(), "E", false);
                this.TEST_Write_Config(DateTime.Now + " : Error in ReadMappings. Couldn't read config: " + exception.ToString());
                mappings.Error = true;
            }
            return mappings;
        }

        [WebMethod]
        public bool Test_AccountLookupVer1(string input, bool isName, string moreKey, bool moreFlag)
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            try
            {
                LookupRequest lookup = new LookupRequest
                {
                    InputData = input,
                    ByNameFlag = isName,
                    MoreRecordsFlag = moreFlag,
                    MoreRecordsKey = moreKey
                };
                this.IG_Account_Lookup_To_PMS(lookup);
            }
            catch (Exception)
            {
            }
            return true;
        }

        [WebMethod]
        public void Test_PMS_AnalList()
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            try
            {
                VertedaDiagnostics.LogMessage("Test_PMS_AnalList started", "I", false);
                ConfigSettings conf = this.ReadConfig();
                if (!conf.Error)
                {
                    PMSLogInResponse response = this.PMS_Login(conf);
                    this.PMS_Analysis_List(response.Session_id);
                }
                else
                {
                    VertedaDiagnostics.LogMessage("Error in Test_PMS_AnalList: Could not get config information", "E", false);
                    this.TEST_Write_Config(DateTime.Now + " : Error in Test_PMS_AnalList: Could not get config information");
                }
            }
            catch (Exception exception)
            {
                this.IGPMS_SendBugsnag("Error in Test_PMS_AnalList", exception, "Database");
                VertedaDiagnostics.LogMessage("Error in Test_PMS_AnalList: " + exception.ToString(), "E", false);
                this.TEST_Write_Config(DateTime.Now + " : Error in Test_PMS_AnalList: " + exception.ToString());
            }
        }

        [WebMethod]
        public void Test_Read_Mappings()
        {
            try
            {
                (from x in this.ReadMappings().RevenueCodes
                 where (x.TypeID == 0) && ((x.ProfitCenter == 0) && ((x.MealID == 0) && (x.ClassID == "Default")))
                 select x.AnalCode).FirstOrDefault<string>().ToString();
            }
            catch (Exception exception)
            {
                this.IGPMS_SendBugsnag("Error in Test_Read_Mappings", exception, "Database");
                VertedaDiagnostics.LogMessage("Error in Test_Read_Mappings: " + exception.ToString(), "E", false);
                this.TEST_Write_Config(DateTime.Now + " : Error in Test_Read_Mappings: " + exception.ToString());
            }
        }

        [WebMethod]
        public string Test_ReadConfig()
        {
            string str = "";
            try
            {
                string str2 = HttpContext.Current.Server.MapPath("~/");
                ConfigSettings settings = this.ReadConfig();
                object[] objArray1 = new object[9];
                objArray1[0] = settings.InterfaceID;
                objArray1[1] = " ";
                objArray1[2] = settings.OperatorCode;
                objArray1[3] = " ";
                objArray1[4] = settings.Password;
                objArray1[5] = " ";
                objArray1[6] = settings.SiteID;
                objArray1[7] = " ";
                objArray1[8] = settings.Url;
                string str3 = string.Concat(objArray1);
                VertedaDiagnostics.LogMessage("Test_ReadConfig " + str3, "I", false);
                this.TEST_Write_Config(DateTime.Now + "Test_ReadConfig " + str3);
                str = "path = " + str2 + "     " + str3;
            }
            catch (Exception)
            {
            }
            return str;
        }

        public void TEST_Write_Config(string input)
        {
            try
            {
                string str = DateTime.Now.ToShortDateString().Replace("/", "_");
                string path = @"C:\IGPMS\" + str + "_IGPMS_Log.txt";
                if (!File.Exists(path))
                {
                    File.Create(path).Dispose();
                }
                using (StreamWriter writer = File.AppendText(path))
                {
                    writer.WriteLine(input);
                    VertedaDiagnostics.LogMessage("TEST_Write_Config wrote: " + input, "I", false);
                }
            }
            catch
            {
            }
        }


    }
}

